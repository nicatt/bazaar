# Used technologies
* **Backend**
1. Python/Django
2. PostgreSQL
* **Frontend**
1. Vue/CLI
2. Vuex
3. Vuetify
* **Deployment**
1. Docker

# Steps to run the app
1. Make sure both <b>Docker</b> and <b>Docker Compose</b> are installed on your OS
2. Give permission to <i>entrypoint.sh</i> by using this command:<br/>
<code>chmod +x ./entrypoint.sh</code>
3. Run Docker Compose file by using <code>docker-compose up --build</code>

# Possible issues
1. If you build Docker more than once, you need to give permission to <i>volumes</i> folder (pgdata, redis etc.).<br />
<code>sudo chmod -R 777 volumes/</code>