FROM python:3.8

ENV PYTHONBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV DOCKER_CONTAINER 1
ENV APP_ROOT /code

RUN mkdir ${APP_ROOT}
COPY . ${APP_ROOT}
WORKDIR ${APP_ROOT}

RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r ${APP_ROOT}/requirements.txt