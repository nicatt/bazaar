#!/bin/bash

MANAGE_PY='./bazaar/manage.py'

# Collect static files
#echo "Collecting static files..."
#python3 ${MANAGE_PY} collectstatic --noinput

# Apply migrations
echo "Applying migrations..."
python3 ${MANAGE_PY} migrate

# Start app
echo "Starting the server..."
python3 ${MANAGE_PY} runserver 0.0.0.0:8000