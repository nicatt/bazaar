from datetime import datetime, timedelta

import pytz
from django.conf import settings
from rest_framework import exceptions
from rest_framework.authentication import TokenAuthentication


class ExpiringTokenAuthentication(TokenAuthentication):
    def authenticate_credentials(self, key):
        try:
            token = self.get_model().objects.get(key=key)
        except self.get_model().DoesNotExist:
            raise exceptions.AuthenticationFailed('Invalid token')

        if not token.user.is_active:
            raise exceptions.AuthenticationFailed('User is inactive or deleted')

        # This is required for the time comparison
        now = datetime.now(pytz.timezone(settings.TIME_ZONE))

        if token.created < now - timedelta(hours=settings.REST_AUTH_TOKEN_LIFE):
            raise exceptions.AuthenticationFailed('Token has expired')

        return token.user, token
