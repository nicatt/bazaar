from rest_framework import routers
from django.urls import path, include
from . import views

router = routers.DefaultRouter()

app_name = 'api'

# Open endpoints
urlpatterns = [
    path('', include(router.urls)),
    path('initialize', views.InitializeApiView.as_view(), name='initialize'),
]

# Protected endpoints (token)
urlpatterns += [
    path('user/login', views.LoginApiView.as_view(), name='user-login'),
    path('user/details', views.UserDetailsApiView.as_view(), name='user-details')
]
