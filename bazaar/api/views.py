from rest_framework import generics, permissions
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers.user import DetailsSerializer, LoginSerializer
from .serializers.misc import InitializerSerializer


# region Users
class UserDetailsApiView(generics.RetrieveAPIView):
    serializer_class = DetailsSerializer

    def get_object(self):
        return self.request.user


class LoginApiView(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data

        response = DetailsSerializer(user).data
        response['token'] = self.update_token(user)

        return Response(response)

    def update_token(self, user):
        Token.objects.get(user=user).delete()
        token = Token.objects.create(user=user)

        return token.key

# endregion


class InitializeApiView(generics.GenericAPIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = InitializerSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user, token = serializer.validated_data

        response = DetailsSerializer(user).data
        response['token'] = token.key
        response['login'] = True

        return Response(response)
