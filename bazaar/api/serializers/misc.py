from rest_framework import serializers


class InitializerSerializer(serializers.Serializer):
    token = serializers.CharField(max_length=40, allow_null=True)

    def validate(self, attrs):
        from bazaar.authentication import ExpiringTokenAuthentication

        auth = ExpiringTokenAuthentication()

        return auth.authenticate_credentials(attrs['token'])
