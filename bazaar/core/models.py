from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=30)
    parent = models.ForeignKey('self',
                               on_delete=models.SET_NULL,
                               blank=True,
                               null=True,
                               default=None,
                               help_text="Leave empty if it is a top category")

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name


class Filter(models.Model):
    name = models.CharField(max_length=30)
    category = models.ManyToManyField(Category)

    def __str__(self):
        return self.name


class Specification(models.Model):
    name = models.CharField(max_length=30)
    filter = models.ForeignKey(Filter, on_delete=models.CASCADE)

    def __str__(self):
        return "{0} - {1}".format(self.filter.name, self.name)
