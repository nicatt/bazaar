from django.contrib import admin
from .models import Category, Filter, Specification


admin.site.register(Category)
admin.site.register(Filter)
admin.site.register(Specification)
