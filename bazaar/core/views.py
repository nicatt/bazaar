from django.views import generic


class CoreIndexView(generic.TemplateView):
    template_name = 'core/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['message'] = "Hello Word!"

        return context
