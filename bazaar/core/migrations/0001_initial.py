# Generated by Django 3.1.2 on 2020-11-07 14:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('parent', models.ForeignKey(default=None, help_text='Leave empty if it is a top category.', null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.category')),
            ],
        ),
    ]
