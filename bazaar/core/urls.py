from django.urls import path
from . import views

urlpatterns = [
    path('', views.CoreIndexView.as_view(), name='core-index'),
]